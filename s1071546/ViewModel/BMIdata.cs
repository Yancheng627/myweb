﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace s1071546.ViewModel
{
    public class BMIdata
    {
        [Display(Name = "體重")]
        [Required(ErrorMessage = "必填欄位")]
        [Range(30, 150, ErrorMessage = "請輸入30-150的數值")]

        public float? Height { get; set; }
        public float? Weight { get; set; }
        public string Level { get; set; }
        public float? Result { get; set; }
    }
}