﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1071546.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["aaa"]="123";
            ViewBag.A = "23";
            ViewBag.B = "26";
            return View();
        }

        public ActionResult Html()
        {
            return View();
        }

        public ActionResult HtmlHelper()
        {
            return View();
        }
        public ActionResult Razor()
        {
            return View();
        }
    }
}