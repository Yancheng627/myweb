﻿using s1071546.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1071546.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIdata());
        }
        [HttpPost]
        public ActionResult Index(BMIdata data)
        {
            if (data.Height < 50 || data.Height > 200)
            {
                ViewBag.HeightError = "身高請輸入50~200的數值";
            }

            if (data.Weight < 30 || data.Weight > 150)
            {
                ViewBag.WeightError = "體重請輸入30~150的數值";
            }

            if (ModelState.IsValid)
            {
                var m_h = data.Height/100;
                var r = data.Weight / (m_h * m_h);

                

                var l = "";

                if (r < 18.5)
                {
                    l = "體重過輕";
                }
                else if (r >= 18.5 && r < 24)
                {
                    l = "體重在正常範圍內";
                }
                else if (r >= 24 && r < 27)
                {
                    l = "體重過重";
                }
                else if (r >= 27 && r < 30)
                {
                    l = "輕度肥胖";
                }
                else if (r >= 30 && r < 35)
                {
                    l = "中度肥胖";
                }
                else if (r > 35)
                {
                    l = "重度肥胖";
                }

                data.Level = l;
                data.Result = r;
            }
            return View(data);
        }
    }
}